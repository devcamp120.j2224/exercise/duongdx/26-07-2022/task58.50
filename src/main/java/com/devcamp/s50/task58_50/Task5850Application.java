package com.devcamp.s50.task58_50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5850Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5850Application.class, args);
	}

}
