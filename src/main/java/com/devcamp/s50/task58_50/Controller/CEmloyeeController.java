package com.devcamp.s50.task58_50.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task58_50.model.CEmployee;
import com.devcamp.s50.task58_50.reposity.IEmloyeeReposity;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CEmloyeeController {
    @Autowired
    IEmloyeeReposity iEmloyeeReposity;
    @GetMapping("/emloyee")
    public ResponseEntity<List<CEmployee>> getAllEmloyee(){
        try
        {
            List<CEmployee> listEmloyee = new ArrayList<CEmployee>();
            iEmloyeeReposity.findAll().forEach(listEmloyee::add);
            return new ResponseEntity<List<CEmployee>>(listEmloyee, HttpStatus.OK);
        } catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
       
    } 
}
