package com.devcamp.s50.task58_50.reposity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task58_50.model.CEmployee;

public interface IEmloyeeReposity extends JpaRepository<CEmployee, Long>{
    
}
