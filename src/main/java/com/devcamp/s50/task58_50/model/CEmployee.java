package com.devcamp.s50.task58_50.model;

import javax.persistence.*;



@Entity
@Table(name = "emloyee")
public class CEmployee {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private long id ;

    @Column(name = "ten")
    private String firstname;
    @Column(name = "ho")
    private String lastname;
    @Column(name = "email")
    private String email ;
    @Column(name = "tuoi")
    private long age ;
    @Column(name = "ngay_cap_nhap")
    private long ngayCapNhat;

    public CEmployee() {
    }

    public CEmployee(long id, String firstname, String lastname, String email, long age, long ngayCapNhat) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.age = age;
        this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public long getAge() {
        return age;
    }
    public void setAge(long age) {
        this.age = age;
    }
    public long getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

}
